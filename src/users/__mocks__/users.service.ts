import { userStub } from "../test/stubs/user.stub";

// This will mock UsersService 
// Make sure, this __mock__ folder is the same folder as your service
// use jest.mock('../users.service'); to use 
// It will automatically add spyOn 
export const UsersService = jest.fn().mockReturnValue({
  getUserById: jest.fn().mockResolvedValue(userStub()),
  getUsers: jest.fn().mockResolvedValue([userStub()]),
  createUser: jest.fn().mockResolvedValue(userStub()),
  updateUser: jest.fn().mockResolvedValue(userStub()),
})