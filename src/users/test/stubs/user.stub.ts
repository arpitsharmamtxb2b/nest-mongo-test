import { User } from "../../schemas/user.schema";

// Returning as a function as it might be used somewhere and could lead into changed values
export const userStub = (): User => {
 return {
    userId: '123',
    email: 'test@example.com',
    age: 23,
    favoriteFoods:  ['apples', 'pizza']
  }
}